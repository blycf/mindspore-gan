# mindspore-gan

#### 介绍
基于MindSpore框架的一些基本gan的实现，主要包括：使用基本GAN拟合正态分布，使用DCGAN生成Mnist手写数字，使用CGAN生成手写数字。

使用的框架版本：Mindspore1.0.1